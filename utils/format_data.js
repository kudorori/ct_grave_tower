const originalData = require('./original.json')
const fs = require('fs')

let output = {}

for ( const item of originalData ) {
    if ( output[item.floor] === undefined  ) {
        output[item.floor] = []
    }
    output[item.floor].push(item)
}

fs.writeFileSync('./format.json', JSON.stringify(output, undefined))