function binarySearch (numberArr, cb) {
  
    // 取得陣列中間的 index
    let middleIndex = Math.floor(numberArr.length / 2)
    
    let position = cb(numberArr[middleIndex])
    // 如果找到就回傳 true
    if (position === 0) {
      return numberArr[middleIndex]
    } 
    
    // 如果還沒找到，而且 numberArr 只剩一個元素，表示找不到
    if(numberArr.length <= 1 ) {
      return undefined
    }
  
    // 如果還沒找到
    if (position > 0) {
      // 且 target 大於中間的數值，則取後半段的陣列再搜尋
      return binarySearch (numberArr.slice(middleIndex, numberArr.length), cb)
    } else if (position < 0) {
      // 且 target 小於中間的數值，則取前半段的陣列再搜尋
      return binarySearch (numberArr.slice(0, middleIndex), cb)
    } 
  }


function getNum(type, num) {
    var target
    if ( data_source === undefined ) {
        data_source = {}
    }

    
    if ( num >= 58714 ) {
        //彌陀
        target = "彌陀";
    } else if ( type === 1 ) {
        target = "蓮位";
    } else if ( type === 2 ) {
        target = "祿位";
    }

    if ( data_source[target] === undefined ) {
        data_source[target] = []
    }

    return binarySearch(data_source[target], function(data) {
        if ( data.start_num > num ) {
            return -1
        } else if ( data.end_num < num ) {
            return 1
        } else if ( (data.start_num <= num && data.end_num >= num)  ) {
            return 0
        }
    })
}